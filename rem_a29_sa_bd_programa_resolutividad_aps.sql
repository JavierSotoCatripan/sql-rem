CREATE OR REPLACE FUNCTION usr_jesoto.rem_a29_sa_programa_resolutividad_aps_consulta()
 RETURNS text
 LANGUAGE plpgsql
AS $function$DECLARE
BEGIN
execute 

$$


--- CONSULTA / CONTROL ES PARA CONSULTA OFTALMOLOGICA
drop table if exists cons_oftal;create temp table cons_oftal as
select nif2,especiali,fecha2,medico_id,1::smallint as consulta_oftalmologica from admomi.iddcop where wproto=383 and wnumero=374 and campo <> '';
create index cons_oftal_i_01 on cons_oftal using btree (nif2,fecha2,medico_id);

--- FONDO DE OJO DM / FONDO DE OJO HTA ES PARA TEGNOLOGO MEDICO OFTALMOLOGIA OTRAS CONSULTA
drop table if exists tm_otras_cons_oftal;create temp table tm_otras_cons_oftal as
select nif2,especiali,fecha2,medico_id,1::smallint as tm_otras_consulta_oftalmologica from admomi.iddcop where wproto=382 and wnumero=156 and campo = 'S' union
select nif2,especiali,fecha2,medico_id,1::smallint as tm_otras_consulta_oftalmologica from admomi.iddcop where wproto=382 and wnumero=173 and campo = 'S';
create index tm_otras_cons_oftal_i_01 on tm_otras_cons_oftal using btree (nif2,fecha2,medico_id);

--- INGRESO GLAUCOMA UAPO
drop table if exists ing_glaucoma_uapo;create temp table ing_glaucoma_uapo as
select nif2,especiali,fecha2,medico_id,campo::smallint as ingreso_glaucoma_uapo from admomi.iddcop where wproto=383 and wnumero=299 and campo = '01';
create index ing_glaucoma_uapo_i_01 on ing_glaucoma_uapo using btree (nif2,fecha2,medico_id);
--TIPRES


--- EGRESO GLAUCOMA UAPO
drop table if exists egr_glaucoma_uapo;create temp table egr_glaucoma_uapo as
select nif2,especiali,fecha2,medico_id,campo::smallint as egreso_glaucoma_uapo from admomi.iddcop where wproto=383 and wnumero=449 and campo <>'';
create index egr_glaucoma_uapo_i_01 on egr_glaucoma_uapo using btree (nif2,fecha2,medico_id);

--- CONTROL GLAUCOMA UAPO
drop table if exists ctr_glaucoma_uapo;create temp table ctr_glaucoma_uapo as
select nif2,especiali,fecha2,medico_id,1::smallint as control_glaucoma_uapo from admomi.iddcop where wproto=383 and wnumero=299 and campo = '02';
create index ctr_glaucoma_uapo_i_01 on ctr_glaucoma_uapo using btree (nif2,fecha2,medico_id);
--TIPRES

--- VIVIO DE REFRACCION
drop table if exists vicio_de_refraccion;create temp table vicio_de_refraccion as
select nif2,especiali,fecha2,medico_id,1::smallint as vicio_de_refraccion from admomi.iddcop where wproto=382 and wnumero=2 and campo = 'S';
create index vicio_de_refraccion_i_01 on vicio_de_refraccion using btree (nif2,fecha2,medico_id);

--- CURVA DE TENSION APLANÁTICA
drop table if exists curva_de_tension_aplanatica;create temp table curva_de_tension_aplanatica as
select nif2,especiali,fecha2,medico_id,1::smallint as curva_de_tension_aplanatica from admomi.iddcop where wproto=383 and wnumero=273 and campo = 'S' union 
select nif2,especiali,fecha2,medico_id,1::smallint as curva_de_tension_aplanatica from admomi.iddcop where wproto=383 and wnumero=286 and campo <>'' union
select nif2,especiali,fecha2,medico_id,1::smallint as curva_de_tension_aplanatica from admomi.iddcop where wproto=383 and wnumero=287 and campo <>'' ;
create index curva_de_tension_aplanatica_i_01 on curva_de_tension_aplanatica using btree (nif2,fecha2,medico_id);

--- PAQUIMETRIA
drop table if exists paquimetria;create temp table paquimetria as
select nif2,especiali,fecha2,medico_id,1::smallint as paquimetria from admomi.iddcop where wproto=383 and wnumero=290 and campo <>'' union
select nif2,especiali,fecha2,medico_id,1::smallint as paquimetria from admomi.iddcop where wproto=383 and wnumero=292 and campo <>'' ;
create index paquimetria_i_01 on paquimetria using btree (nif2,fecha2,medico_id);


---IC GENERADAS
drop table if exists derivado_mtc_uapo;create temp table derivado_mtc_uapo as
-- otro_establecimiento  ANCORA Madre Teresa de Calcuta (UAPO)
select nif2,especiali,fecha2,medico_id,campo as cod_centro from admomi.iddcop where wproto=332 and wnumero=100 and campo = '143231';
create index derivado_mtc_uapo_i_01 on derivado_mtc_uapo using btree (nif2,fecha2,medico_id);

drop table if exists especialidad_oftal;create temp table especialidad_oftal as
-- otra_especialidad Oftalmologia
select nif2,especiali,fecha2,medico_id,campo as cod_especiali from admomi.iddcop where wproto=332 and wnumero=99 and campo = '07-400-9';
create index especialidad_oftal_i_01 on especialidad_oftal using btree (nif2,fecha2,medico_id);

--TABLA ECHOS
drop table if exists bd1;create temp table bd1 as
select nif2,especiali,fecha2,medico_id from derivado_mtc_uapo union
select nif2,especiali,fecha2,medico_id from especialidad_oftal ;
create index bd1_i_01 on bd1 using btree (nif2,fecha2,medico_id);

drop table if exists ic_derivadas_uapo_mtc;create temp table ic_derivadas_uapo_mtc as
select distinct a.nif2,a.especiali,a.fecha2,a.medico_id
	, case when a1.cod_centro ='143231' then 'ANCORA Madre Teresa de Calcuta' end centro_derivado
	,case when a2.cod_especiali='07-400-9'  then 'Oftalmologia' end especialidad
	,1::smallint as q
from bd1 as a --3116
left join derivado_mtc_uapo as a1 using (nif2,fecha2,especiali,medico_id)
left join especialidad_oftal as a2 using (nif2,fecha2,especiali,medico_id);
create index ic_derivadas_uapo_mtc_i_01 on ic_derivadas_uapo_mtc using btree (nif2,fecha2,medico_id);

--select distinct a.nif2,a.especiali,a.fecha2,a.medico_id	,a.centro_derivado,a.especialidad,a.q from bd2 as a;


-- DIMENCIONES 
--recepcion de paciente
drop table if exists dim_ing_glaucoma;create temp table dim_ing_glaucoma as
select codigo::smallint,descripcio as texto from admomi.iddtau where tabla ='TIPRES' ;

drop table if exists e_bd_rem_29;create temp table e_bd_rem_29 as
select nif2,especiali,fecha2,medico_id from cons_oftal 			union
select nif2,especiali,fecha2,medico_id from tm_otras_cons_oftal  	union
select nif2,especiali,fecha2,medico_id from ing_glaucoma_uapo 		union
select nif2,especiali,fecha2,medico_id from egr_glaucoma_uapo 		union
select nif2,especiali,fecha2,medico_id from ctr_glaucoma_uapo 		union
select nif2,especiali,fecha2,medico_id from vicio_de_refraccion		union
select nif2,especiali,fecha2,medico_id from curva_de_tension_aplanatica union
select nif2,especiali,fecha2,medico_id from paquimetria union
select nif2,especiali,fecha2,medico_id from ic_derivadas_uapo_mtc	;
create index e_bd_rem_29_i_01 on e_bd_rem_29 using btree (nif2,fecha2,medico_id);

drop table if exists bd_rem_29;create temp table bd_rem_29 as
select distinct  a.nif2
	,b.sexo
	,edad_en_agnios (a.fecha2,b.nacimiento) as edad
	,b.centro
	,a.especiali,a.fecha2,a.medico_id
	,(select b.medico from admomi.iddmed as b where a.medico_id=b.id limit 1) as medico
	,(select b.med_estamento_desc from admomi.iddmed as b where a.medico_id=b.id limit 1) as estamento
	,coalesce (a1.consulta_oftalmologica,0) 		as q_consulta_oftalmologica
	,coalesce (a2.tm_otras_consulta_oftalmologica,0) 	as q_tm_otras_consulta_oftalmologica
	,coalesce (a3.ingreso_glaucoma_uapo,0) 			as q_ingreso_glaucoma_uapo
	,coalesce (a4.egreso_glaucoma_uapo,0) 			as q_egreso_glaucoma_uapo 
	,coalesce (a5.control_glaucoma_uapo,0) 			as q_control_glaucoma_uapo
	,coalesce (a6.vicio_de_refraccion,0) 			as q_vicio_de_refraccion
	,coalesce (a7.q ,0) 							as q_ic_generadas
	,coalesce (a8.curva_de_tension_aplanatica,0) 	as q_curva_de_tension_aplanatica
	,coalesce (a9.paquimetria,0)                    as q_paquimetria
from e_bd_rem_29 		as a
left join cons_oftal 		as a1 using (nif2,especiali,fecha2,medico_id)
left join tm_otras_cons_oftal 	as a2 using (nif2,especiali,fecha2,medico_id)
left join ing_glaucoma_uapo 	as a3 using (nif2,especiali,fecha2,medico_id)
left join egr_glaucoma_uapo 	as a4 using (nif2,especiali,fecha2,medico_id)
left join ctr_glaucoma_uapo 	as a5 using (nif2,especiali,fecha2,medico_id)
left join vicio_de_refraccion 	as a6 using (nif2,especiali,fecha2,medico_id)
left join ic_derivadas_uapo_mtc as a7 using (nif2,especiali,fecha2,medico_id)
left join curva_de_tension_aplanatica as a8 using (nif2,especiali,fecha2,medico_id)
left join paquimetria as a9 using (nif2,especiali,fecha2,medico_id)
left join admomi.iddpacpa3 as b on a.nif2=b.nif2;
create index bd_rem_29_i_01 on bd_rem_29 using btree (nif2,fecha2,medico_id);

drop table if exists usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps;create table usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps as 
select distinct a.nif2
	,a.sexo
	,a.edad
	,case when edad <15 then '.MENOR DE 15a' when edad >=15 then 'DE 15 Y MAS' end menor_15_y_15_y_mas
	,(select case 	when a.sexo=b.sexo_h and a.edad=b.agnios then rango_edad_h
			when a.sexo=b.sexo_m and a.edad=b.agnios then rango_edad_m
				end rango_y_sexo
			from usr_jesoto.rango_rem_a as b
			where a.edad=b.agnios
			and hoja =29
			and seccion='A'
			limit 1
	) as rango
	
	,a.centro
	,a.especiali
	,a.fecha2
	,aym (a.fecha2) as aym
	,a.medico_id 
	,a.medico
	,a.estamento
	,a.q_consulta_oftalmologica
	,a.q_tm_otras_consulta_oftalmologica
	,a.q_ingreso_glaucoma_uapo
	,a.q_egreso_glaucoma_uapo 
	,a.q_control_glaucoma_uapo
	,a.q_vicio_de_refraccion
	,a.q_ic_generadas
	,a.q_ingreso_glaucoma_uapo as q_consulta_descarte_glaucoma_uapo
	,a.q_curva_de_tension_aplanatica
	,a.q_paquimetria
from bd_rem_29 as a 
order by fecha2 desc ;

create index rem_a29_sa_bd_programa_resolutividad_aps_i_01 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (nif2);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_02 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (sexo);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_03 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (edad);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_04 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (menor_15_y_15_y_mas);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_05 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (rango);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_06 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (centro);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_07 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (especiali);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_08 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (fecha2);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_09 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (aym);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_10 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (medico_id);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_11 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (medico);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_12 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (estamento);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_13 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (q_consulta_oftalmologica);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_14 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (q_tm_otras_consulta_oftalmologica);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_15 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (q_ingreso_glaucoma_uapo);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_16 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (q_egreso_glaucoma_uapo);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_17 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (q_control_glaucoma_uapo);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_18 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (q_vicio_de_refraccion);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_19 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (q_ic_generadas);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_20 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (q_consulta_descarte_glaucoma_uapo);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_21 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (q_curva_de_tension_aplanatica);
create index rem_a29_sa_bd_programa_resolutividad_aps_i_22 on usr_jesoto.rem_a29_sa_bd_programa_resolutividad_aps using btree (q_paquimetria);

$$

;
RETURN 'OK';
END;
$function$
;
